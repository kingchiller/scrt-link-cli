#!/usr/bin/env node

import chalk from "chalk";
import clear from "clear";
import figlet from "figlet";
import standard from "figlet/importable-fonts/Bloody.js";
import { Command, Option } from "commander";

import { createSecret } from "scrt-link-core";

const program = new Command();

figlet.parseFont("Standard", standard);

// import { createSecret } from "scrt-link-core";

// @todo
clear();
console.log(
  chalk.hex("#ff0083")(
    figlet.textSync("scrt.link", { horizontalLayout: "full" })
  )
);

const handleAction = async (message: string, options: any) => {
  console.log(`Processing…`);
  const { secretLink } = await createSecret(message, {
    password: options.password,
    secretType: options.type,
  });
  console.log(chalk.green("Done."));
  console.log(chalk.hex("#ff0083")(`Your secret link:  ${secretLink}`));
};

program
  .version("0.1.0")
  .description("Securely share sensitive information online.")
  .command("create")
  .argument("<message>", "integer argument")
  .addOption(
    new Option("-t, --type <type>", "Secret type").choices([
      "text",
      "link",
      "neogram",
    ])
  )
  .option("-pw, --password", "Custom password")
  .action(handleAction);

program.parseAsync(process.argv);
