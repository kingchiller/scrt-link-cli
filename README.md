# scrt-link-cli

[scrt.link](https://scrt.link) lets you share sensitive information online. End-to-end encrypted. One time.

This package allows you to use the service programmatically using the command line.

## Usage

### Basic usage

```bash
npm i -g scrt-link-cli

scrt create "Some confidential information…"
# -> https://scrt.link/l/CWmbcLtxzFRad8JJ#ReCMTkJkAtUqFF9ydBAWdYaz
```
